
Koha data import with vufind harvester

- Copy and extract vufindharvest-master folder to drupal folder 
- Go to vufindharvest-master folder
- Run harvester script
   $sudo php bin/harvest_oai.php --url=http://koha.nvli.in/cgi-bin/koha/oai.pl --metadataPrefix=marc21 --harvestedIdLog=harvest.log koha_data
- Copy and extract koha-import folder to drupal modules directory
- Change file owner permission to www-data:www-data
    $sudo chown www-data:www-data koha_import
- Install module koha-import
- Run cron job from drupal interface



